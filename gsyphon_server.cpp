/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2019 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 */

/*
 * File:   gsyphon_server.cpp
 * Author: frankiezafe
 *
 * Created on February 14, 2019, 11:41 PM
 */

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * THIS CLASS IS A PLACEHOLDER FOR PROJECTS USING GSYPHON ON LINUX OR WINDOWS
 * it does absolutly nothing except holding the parameters
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

#include "gsyphon_server.h"

GSyphonServer::GSyphonServer( ) : 
server_active(true),
viewport_texture(0)
{

	mSyphon = 0;
	server_name = DEFAULT_SERVER_NAME;
	connect("texture_changed", this, "_sig_texture_changed");
	init_server();

}

GSyphonServer::~GSyphonServer( ) {
	stop_server();
}

void GSyphonServer::_sig_texture_changed() {
	viewport_texture = Object::cast_to< ViewportTexture >( get_texture().ptr() );
	std::cout << "texture_changed >> GSyphonServer::_sig_texture_changed" << std::endl;

}

void GSyphonServer::init_server() {
	// nothing to do here
}

void GSyphonServer::stop_server() {
	// nothing to do here
}

void GSyphonServer::set_server_active( bool b ) {
	server_active = b;
}

bool GSyphonServer::get_server_active() const {
	return server_active;
}

void GSyphonServer::set_server_name( gString gs ) {

	if ( server_name == gs ) {
		return;
	}
	server_name = gs;
	init_server();

}
	
gString GSyphonServer::get_server_name() {
	return server_name;
}

void GSyphonServer::publish() {
	// nothing to do here
}

void GSyphonServer::_sig_idle_frame() {
	//publish();
}

void GSyphonServer::_notification(int p_what) {

	Sprite::_notification( p_what );

	// std::cout << "GSyphonServer notification: " << p_what << std::endl;

	switch (p_what) {
			case NOTIFICATION_PROCESS: {
				// publish();
			} break;
			case MainLoop::NOTIFICATION_WM_QUIT_REQUEST:
			case NOTIFICATION_EXIT_TREE: {
				//get_tree()->disconnect( "idle_frame", this, "_sig_idle_frame" );
				server_active = false;
				stop_server();
			} break;
			case NOTIFICATION_ENTER_TREE: {
				//get_tree()->connect( "idle_frame", this, "_sig_idle_frame" );
			} break;
	}

}

void GSyphonServer::_bind_methods() {

	ClassDB::bind_method(D_METHOD("_sig_texture_changed"), &GSyphonServer::_sig_texture_changed);
	ClassDB::bind_method(D_METHOD("_sig_idle_frame"), &GSyphonServer::_sig_idle_frame);

	ClassDB::bind_method(D_METHOD("set_server_active", "server_active"), &GSyphonServer::set_server_active);
	ClassDB::bind_method(D_METHOD("get_server_active"), &GSyphonServer::get_server_active);

	ClassDB::bind_method(D_METHOD("set_server_name", "server_name"), &GSyphonServer::set_server_name);
	ClassDB::bind_method(D_METHOD("get_server_name"), &GSyphonServer::get_server_name);

	ClassDB::bind_method(D_METHOD("publish"), &GSyphonServer::publish);

	ADD_GROUP("Syphon server", "syphon_");
	ADD_PROPERTY(PropertyInfo(Variant::STRING, "server_name"), "set_server_name", "get_server_name");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "server_active"), "set_server_active", "get_server_active");
	ADD_PROPERTY(PropertyInfo(Variant::STRING, "server_name"), "set_server_name", "get_server_name");

}