/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2019 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 */

/*
 * File:   gsyphon_server.h
 * Author: frankiezafe
 *
 * Created on February 14, 2019, 11:35 PM
 */

#ifndef GSYPHON_SERVER_H
#define GSYPHON_SERVER_H

#include <iostream>
#include <vector>

#include "servers/visual_server.h"
#include "scene/main/node.h"
#include "scene/main/viewport.h"
//#include "drivers/gl_context/context_gl.h"
#include "scene/3d/visual_instance.h"
#include "scene/2d/sprite.h"
#include "gcommon.h"

#define DEFAULT_SERVER_NAME "__GODOT__"

class GSyphonServer : public Sprite {
	GDCLASS( GSyphonServer, Sprite );

public:

	GSyphonServer( );

	virtual ~GSyphonServer( );

	void set_server_active( bool b );
	bool get_server_active() const;

	void set_server_name( gString m );
	gString get_server_name();

	void publish();

protected:

	static void _bind_methods( );
	void _notification(int p_what);
	void _sig_texture_changed();
	void _sig_idle_frame();

	void init_server();
	void stop_server();

	gString server_name;
	bool server_active;
	ViewportTexture* viewport_texture;

	void *mSyphon;

};

#endif /* GSYPHON_SERVER_H */
