/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2019 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 */

/*
 * File:   gsyphon_server.mm
 * Author: frankiezafe
 *
 * Created on February 14, 2019, 11:41 PM
 */

#include "gsyphon_server.h"

#import <Cocoa/Cocoa.h>
#import <Syphon/Syphon.h>
#import <OpenGL/CGLMacro.h>

// static std::vector< GSyphonServer* > _servers;
// static Mutex* _server_mutex = 0;

GSyphonServer::GSyphonServer( ) : 
server_active(true),
viewport_texture(0)
{

    std::cout << "GSyphonServer!!!!!" << std::endl;
    mSyphon = nil;
    server_name = DEFAULT_SERVER_NAME;
    connect("texture_changed", this, "_sig_texture_changed");
    init_server();

}

GSyphonServer::~GSyphonServer( ) {
    stop_server();
}

void GSyphonServer::_sig_texture_changed() {
    // server_active = false;
    viewport_texture = Object::cast_to< ViewportTexture >( get_texture().ptr() );
    std::cout << "texture_changed >> GSyphonServer::_sig_texture_changed" << std::endl;

}

void GSyphonServer::init_server() {

    std::wstring ws = server_name.c_str();
    std::string n(ws.begin(), ws.end());
    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	NSString *title = [NSString stringWithCString:n.c_str()
										 encoding:[NSString defaultCStringEncoding]];
	if (!mSyphon) {
        // NSDictionary *options = @{SyphonServerOptionDepthBufferResolution: @24};
		// mSyphon = [[SyphonServer alloc] initWithName:title context:CGLGetCurrentContext() options:options];
        mSyphon = [[SyphonServer alloc] initWithName:title context:CGLGetCurrentContext() options:nil];
	} else {
		[(SyphonServer *)mSyphon setName:title];
	}
    [pool drain];

}

void GSyphonServer::stop_server() {

    if ( !mSyphon ) {
        return;
    }

    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
    [(SyphonServer *)mSyphon stop];
    [(SyphonServer *)mSyphon release];
    mSyphon = nil;
    [pool drain];
}

void GSyphonServer::set_server_active( bool b ) {
    server_active = b;
}

bool GSyphonServer::get_server_active() const {
    return server_active;
}

void GSyphonServer::set_server_name( gString gs ) {

    if ( server_name == gs ) {
        return;
    }
    server_name = gs;
    init_server();

}
	
gString GSyphonServer::get_server_name() {
    return server_name;
}

void GSyphonServer::publish() {

    Ref<Texture> t = get_texture();

    if ( t.is_null() || !server_active ) {
        return;
    }

    int x = 0;
    int y = 0;
    int w = t->get_width();
    int h = t->get_height();
    int full_w = t->get_width();
    int full_h = t->get_height();
    uint32_t tID = 0;
    int tTAR = 0;

    if ( viewport_texture ) {
        // std::cout << "ViewportTexture !!!!! " << vt << " / " << vp << std::endl;
        tID = VS::get_singleton()->texture_get_texid( viewport_texture->get_viewport_texture_rid() );
        tTAR = VS::get_singleton()->texture_get_type( viewport_texture->get_viewport_texture_rid() );
    } else {
        tID = VS::get_singleton()->texture_get_texid( t->get_rid() );
        tTAR = VS::get_singleton()->texture_get_type( t->get_rid() );
    }

    if ( is_region() ) {
        Rect2 r = get_region_rect();
        x = r.get_position().x;
        y = r.get_position().y;
        w = r.get_size().x;
        h = r.get_size().y;
    }

    switch( VS::get_singleton()->texture_get_type( t->get_rid() ) ) {
        case 0: // VS::TEXTURE_TYPE_2D:
            tTAR = GL_TEXTURE_2D;
            break;
        case 1: // VS::TEXTURE_TYPE_CUBEMAP:
            tTAR = GL_TEXTURE_CUBE_MAP;
            break;
        case 2: // VS::GL_TEXTURE_2D_ARRAY:
            // tTAR = GL_TEXTURE_2D_ARRAY;
            break;
        case 3: // VS::TEXTURE_TYPE_3D:
            // tTAR = GL_TEXTURE_3D;
            break;
        default:
            break;

    }

    if ( w == 0 || h == 0 || tID == 0 || tTAR == 0 ) {
        // std::cout << "GSyphonServer::publish, invalid texture construction, stopping publishing:" << std::endl;
        // std::cout << 
        //     "\tw: " << w << std::endl <<
        //     "\th: " << h << std::endl <<
        //     "\tid: " << tID << std::endl <<
        //     "\ttarget: " << tTAR << std::endl;
        server_active = false;
        return;
    }

    if (!mSyphon) {
        init_server();
    }

    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
    [(SyphonServer *)mSyphon publishFrameTexture:tID 
        textureTarget:tTAR 
        imageRegion:NSMakeRect(0,0,full_w,full_h) 
        textureDimensions:NSMakeSize(full_w,full_h) 
        flipped:true];
    // texture cropping is not correctly supported in current syphon implementation
    // [(SyphonServer *)mSyphon publishFrameTexture:tID 
    //     textureTarget:tTAR 
    //     imageRegion:NSMakeRect(x,y,w,h) 
    //     textureDimensions:NSMakeSize(full_w-x,full_h-y) 
    //     flipped:true];
    [pool drain];

}

void GSyphonServer::_sig_idle_frame() {
    publish();
    // std::cout << "_sig_idle_frame " << _sig_count << std::endl;
    // _sig_count++;
}

void GSyphonServer::_notification(int p_what) {

    Sprite::_notification( p_what );

    // std::cout << "GSyphonServer notification: " << p_what << std::endl;

    switch (p_what) {
            case NOTIFICATION_PROCESS: {
                // publish();
            } break;
            case MainLoop::NOTIFICATION_WM_QUIT_REQUEST:
            case NOTIFICATION_EXIT_TREE: {
                get_tree()->disconnect( "idle_frame", this, "_sig_idle_frame" );
                server_active = false;
                stop_server();
            } break;
            case NOTIFICATION_ENTER_TREE: {
                get_tree()->connect( "idle_frame", this, "_sig_idle_frame" );
            } break;
    }

    // MainLoop::
    //NOTIFICATION_WM_MOUSE_ENTER = 2,
    //NOTIFICATION_WM_MOUSE_EXIT = 3,
    //NOTIFICATION_WM_FOCUS_IN = 4,
    //NOTIFICATION_WM_FOCUS_OUT = 5,
    //NOTIFICATION_WM_QUIT_REQUEST = 6,
    //NOTIFICATION_WM_GO_BACK_REQUEST = 7,
    //NOTIFICATION_WM_UNFOCUS_REQUEST = 8,
    //NOTIFICATION_OS_MEMORY_WARNING = 9,
    //NOTIFICATION_TRANSLATION_CHANGED = 90,
    //NOTIFICATION_WM_ABOUT = 91,
    //NOTIFICATION_CRASH = 92,
    //NOTIFICATION_OS_IME_UPDATE = 93,

    // Node::
    //NOTIFICATION_ENTER_TREE = 10,
    //NOTIFICATION_EXIT_TREE = 11,
    //NOTIFICATION_MOVED_IN_PARENT = 12,
    //NOTIFICATION_READY = 13,
    //NOTIFICATION_PAUSED = 14,
    //NOTIFICATION_UNPAUSED = 15,
    //NOTIFICATION_PHYSICS_PROCESS = 16,
    //NOTIFICATION_PROCESS = 17,
    //NOTIFICATION_PARENTED = 18,
    //NOTIFICATION_UNPARENTED = 19,
    //NOTIFICATION_INSTANCED = 20,
    //NOTIFICATION_DRAG_BEGIN = 21,
    //NOTIFICATION_DRAG_END = 22,
    //NOTIFICATION_PATH_CHANGED = 23,
    //NOTIFICATION_TRANSLATION_CHANGED = 24,
    //NOTIFICATION_INTERNAL_PROCESS = 25,
    //NOTIFICATION_INTERNAL_PHYSICS_PROCESS = 26,
    //NOTIFICATION_POST_ENTER_TREE = 27,

    // CanvasItem::
    //NOTIFICATION_TRANSFORM_CHANGED = SceneTree::NOTIFICATION_TRANSFORM_CHANGED, //unique
    //NOTIFICATION_DRAW = 30,
    //NOTIFICATION_VISIBILITY_CHANGED = 31,
    //NOTIFICATION_ENTER_CANVAS = 32,
    //NOTIFICATION_EXIT_CANVAS = 33,
    //NOTIFICATION_LOCAL_TRANSFORM_CHANGED = 35,
    //NOTIFICATION_WORLD_2D_CHANGED = 36,

    // Spatial
    //NOTIFICATION_TRANSFORM_CHANGED = SceneTree::NOTIFICATION_TRANSFORM_CHANGED,
    //NOTIFICATION_ENTER_WORLD = 41,
    //NOTIFICATION_EXIT_WORLD = 42,
    //NOTIFICATION_VISIBILITY_CHANGED = 43,
    //NOTIFICATION_LOCAL_TRANSFORM_CHANGED = 44,

}

void GSyphonServer::_bind_methods() {

    ClassDB::bind_method(D_METHOD("_sig_texture_changed"), &GSyphonServer::_sig_texture_changed);
    ClassDB::bind_method(D_METHOD("_sig_idle_frame"), &GSyphonServer::_sig_idle_frame);

    ClassDB::bind_method(D_METHOD("set_server_active", "server_active"), &GSyphonServer::set_server_active);
    ClassDB::bind_method(D_METHOD("get_server_active"), &GSyphonServer::get_server_active);

    ClassDB::bind_method(D_METHOD("set_server_name", "server_name"), &GSyphonServer::set_server_name);
    ClassDB::bind_method(D_METHOD("get_server_name"), &GSyphonServer::get_server_name);

    ClassDB::bind_method(D_METHOD("publish"), &GSyphonServer::publish);

    ADD_GROUP("Syphon server", "syphon_");
    ADD_PROPERTY(PropertyInfo(Variant::STRING, "server_name"), "set_server_name", "get_server_name");
    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "server_active"), "set_server_active", "get_server_active");
    ADD_PROPERTY(PropertyInfo(Variant::STRING, "server_name"), "set_server_name", "get_server_name");

}