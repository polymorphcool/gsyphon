# gSyphon

syphon addon for godot engine, osx only

## godot hack

to enable viewport texture sharing, you have to modify godot core source:

### scene/main/viewport.h

add the public method **get_vp_rid** to ViewportTexture

'''
virtual RID get_viewport_texture_rid() const;
'''

### scene/main/viewport.cpp

add the method somewhere, near other *ViewportTexture* would be nicer

'''
RID ViewportTexture::get_viewport_texture_rid() const {
	ERR_FAIL_COND_V(!vp, RID());
	return vp->texture_rid;
}
'''

and recompile the enigne

### why?

*ViewportTexture* contains a proxy to the actual rendering of the viewport. Godot knows how to handle this, but there is no easy way for an external object such as GSyphonServer to access this rendering. Adding the *get_vp_rid* method allows this access.

## reference 

- https://github.com/Syphon/Syphon-Framework/releases/download/3/Syphon.SDK.3.zip
- https://github.com/astellato/ofxSyphon
- http://syphon.v002.info
- https://github.com/Syphon/syphon-framework

about objective c & scons

- https://github.com/SCons/scons/wiki/MacOSX
- http://www.howtobuildsoftware.com/index.php/how-do/bj43/objective-c-osx-scons-scons-and-objc-
- https://rpg.hamsterrepublic.com/ohrrpgce/Compiling_in_Mac_OS_X (loaction of frameworks)
- https://o440.wordpress.com/2011/02/27/scons-and-osx-framework/
- https://discussions.apple.com/thread/3754858
- https://stackoverflow.com/questions/12228382/after-install-xcode-where-is-clang
- https://clang.llvm.org/docs/ClangCommandLineReference.html

echo $PATH
PATH=$PATH:~/Library/Frameworks
PATH=$PATH:~/Library/Frameworks/Syphon.framework/Versions/Current

- https://stackoverflow.com/questions/38705704/compile-objective-c-with-clang-on-macos-sierra
- https://stackoverflow.com/questions/24131476/compiling-and-linking-swift-plus-objective-c-code-from-the-os-x-command-line

## debugs

2019 02 15

```bash
Undefined symbols for architecture x86_64:
  "_OBJC_CLASS_$_SyphonServer", referenced from:
      objc-class-ref in libmodules.osx.tools.64.a(gsyphon_server.osx.tools.64.o)
ld: symbol(s) not found for architecture x86_64
```

## scub notes

```bash
# env_gsyphon.Append(LINKFLAGS=['-F'+gsyphon_framework_path, '-framework', 'Syphon'])
# env_gsyphon.Append(LINKFLAGS=['-framework', 'Cocoa', '-framework', 'Carbon', '-framework', 'OpenGL', '-framework', 'AGL', '-framework', 'AudioUnit', '-framework', 'CoreAudio', '-framework', 'CoreMIDI', '-lz', '-framework', 'IOKit', '-framework', 'ForceFeedback', '-framework', 'CoreVideo'])

#env_gsyphon.Append(LINKFLAGS=['-ObjC++'])
#env_gsyphon.Append(LINKFLAGS=['-I'+gsyphon_framework_path, '-lsyphon', '-lobjc++'])
#env_gsyphon.Append(CPPFLAGS=['-lobjc'])
#env_gsyphon.Append(CPPFLAGS=['-I/Users/julianatnicolela/Library/Frameworks/Syphon.framework/Versions/Current/Headers'])
#env_gsyphon.Append(LINKFLAGS=['-framework', 'Syphon'])
#env_gsyphon.Append(LINKFLAGS=['-lobjc++', '-framework', 'Syphon'])
#env_gsyphon.Append(LINKFLAGS=['-lobjc-arc', '-framework', 'Syphon'])
#env_gsyphon.Append(LINKFLAGS=['objective-c', '-framework', 'Syphon'])
#env_gsyphon.Append(LINKFLAGS=['-L/Users/julianatnicolela/Library/Frameworks/Syphon.framework/Versions/Current/'])
#env_gsyphon.add_source_files('/Users/julianatnicolela/forge.godot/godot/modules/gsyphon/libs/Syphon/lib/osx/Syphon.framework/src', '*.m')
#env_gsyphon.Append(CPPDEFINES=['SYPHON_UNIQUE_CLASS_NAME_PREFIX=name'])
#env_gsyphon.Append(CPPFLAGS=['-I/Users/julianatnicolela/forge.godot/godot/modules/gsyphon/libs/Syphon/lib/osx/Syphon.framework/src'])
#env_gsyphon.Append(CPPFLAGS=['-I/Users/julianatnicolela/forge.godot/godot/modules/gsyphon/libs/Syphon/lib/osx/Syphon.framework/Headers'])
#env_gsyphon.Append(CPPFLAGS=['-I/Users/julianatnicolela/forge.godot/godot/modules/gsyphon/libs/Syphon'])
#env_gsyphon.Append(CPPFLAGS=['-I/Users/julianatnicolela/forge.godot/godot/modules/gsyphon/Syphon-Framework'])

# Import('env')
# Import('env_modules')
# module_env = env.Clone()
# module_env.add_source_files(env.modules_sources,'*.cpp') # just add all cpp files to the build
# inspired by https://github.com/antarktikali/godot-raknet-example/blob/master/gdraknet/SCsub
# module_env.Append(CPPFLAGS=['-lstdc++'])
# module_env.Append(CPPFLAGS=['-std=gnu++11'])
# module_env.Append(CPPFLAGS=['-I/usr/include/lo', '-llo'])
# module_env.Append(CPPFLAGS=['-lpthread'])
# linux
# module_env.Append(LINKFLAGS=['-lstdc++'])
# module_env.Append(LINKFLAGS=['-std=gnu++11'])
# module_env.Append(LINKFLAGS=['-I/usr/include/lo', '-llo'])
# module_env.Append(LINKFLAGS=['-lpthread'])

# resources: https://mail.python.org/pipermail/pythonmac-sig/2007-February/018672.html

# compilation
# module_env.Append(CPPFLAGS=[''])
# -I/Library/Frameworks/Python.framework/Headers

# linking
# module_env.Append(LINKFLAGS=[''])
# -L/Users/nies/src/boost_1_33_1/bin/boost/libs/python/build/libboost_python.dylib/darwin/release/shared-linkable-true/
```